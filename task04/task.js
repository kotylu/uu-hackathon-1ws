/*
Navrhněte a vytvořte program (funkce main v souboru task.js), který pro vygenerovaný seznam zaměstnanců zjistí nejčastější křestní jména v rámci:

    všech zaměstnanců
    žen
    mužů
    žen na zkrácený úvazek (tj. 10, 20 či 30h/týdně)
    mužů na plný pracovní úvazek (tj. 40h/týdně)
*/
//@@viewOff:const
//@@viewOff:const

//@@viewOn:helpers
//@@viewOff:helpers

const initResult = () => ({
    all: {},
    female: {},
    male:{},
    femalePartTime: {},
    maleFullTime: {}
  });

const incrementItemCount = (item, group, object) => {
    if (!object[group][item.toLowerCase()]) {
      object[group][item.toLowerCase()] = 0;
    }
    object[group][item.toLowerCase()]++;
};

const isMale = (person) => (person.gender === "male");

const getFormatForGraph = (result) => {
  const names = result;
  Object.keys(result).map(groupName => {
    const group = Object.keys(result[groupName]).map(key => ({label: key, value: result[groupName][key]}));
    group.sort((a, b) => {
      if (a.value < b.value) return 1;
      if (a.value > b.value) return -1;
      if (a.value === b.value) return 0;
    });
    result[groupName] = group;
  });
  return {
    names,
    chartData: result
  };
};
//@@viewOn:main
/**
 * @param {object} dtoIn input data
 * @return {object} output data
**/
function main(dtoIn=[]) {
  const inputList = dtoIn;

  const result = initResult();

  inputList.map(item => {
    const name = item.name;
    incrementItemCount(name, "all", result);
    if (isMale(item)) {
      incrementItemCount(name, "male", result);
      if (item.workload === 40) {
        incrementItemCount(name, "maleFullTime", result);
      }
    }
    else {
      incrementItemCount(name, "female", result);
      if (item.workload <= 30) {
        incrementItemCount(name, "femalePartTime", result);
      }
    }
  });

  const formated = getFormatForGraph(result);

  return formated;
}
//@@viewOff:main