//@@viewOff:const
//@@viewOff:const

//@@viewOn:helpers
  /**
   * Returns random number in range <min,max>
   * 
   * @param {number} min min value
   * @param {number} max max value
   * @return {number} random number
  **/
  function getRandom(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  } 
//@@viewOff:helpers

//@@viewOn:main
/**
 * @param {object} dtoIn input data
 * @return {array} output data
**/
function main(dtoIn={}) {

//1

let datum = new Date(Date.now());

//2

let gender = ["male", "female"];

//3

let nameF = ["Jana", "Marie", "Zdenka", "Karolina", "Petra", "Patricie", "Nikola", "Kamila",
"Karla", "Lenka", "Monika", "Kateřina", "Pavla", "Alena", "Alžběta", "Anna", "Ekatarina", "Vera", "Hana", "Ludmila", "Lada",  
"Jana", "Marie", "Zdenka", "Karolina", "Petra", "Patricie"];

//4

let nameM = ["Karel", "Zdenek", "Jan", "Karolin", "Petr", "Patrik", "Kamil", "Pavel", "Aron", "Ales", "Josef", "Ondrej", "Marek", "Martin", "Marcel", "Jaroslav",
"Tomas", "Michal", "Jakub", "Vladimir", "Roman", "Ondrej", "Stanislav", "Daniel", "Antonin", "Vojtech", "Karel", "Zdenek", "Jan", "Karolin", "Petr" ];


//5

let surnameF = ["Fialova", "Zelena", "Modra", "Cervena", "Kvetnova", "Lednova", "Unorova", "Breznova", "Dubnova", "Kvetnova", "Cervnova", "Cervencova", "Srpnova", 
"Zariova", "Rijnova", "Listopadova", "Prosincova", "Oranzova", "Cervena", "Ruzova", "Zlata", "Stribrna", "Hneda", "Novakova", "Haluzova", "Hroznova", "Grepova" ];



//6


let surnameM = ["Fiala", "Zeleny", "Modry", "Cerveny", "Ruzovy", "Zlaty", "Oranzovy", "Stribrny", "Hnedy", "Novak", "Haluza", "Hrozen", "Grep", "Leden", "Unor", 
"Brezen", "Duben", "Kveten", "Cerven", "Cervenec", "Srpen", "Zari", "Rijen", "Listopad", "Prosinec", "Maly"]

//7

let workload = [10, 20, 30, 40]


//8.1

let rok = datum.getFullYear();

//8.2

let mesic= datum.getMonth();

//8.3

let rokHledanyMin = rok - dtoIn.age.min;





//8.4

let rokHledanyMax = rok - dtoIn.age.max;




//8.5
let pomocna = "0";
let mesicHledanyMin = mesic < 10 ? pomocna.concat(mesic + 1) : mesic + 1;






//8.6

let mesicHledanyMax = mesic + (12 - mesic) ;




//8.7

let minDatum = new Date(rokHledanyMin + "-" + mesicHledanyMin) ;   //format: "2020-04-10T17:14:00" 



//8.8


let maxDatum = new Date(rokHledanyMax + "-" + mesicHledanyMax) ;   //format: "2020-04-10T17:14:00" 




function randomDate(date1, date2){
    function randomValueBetween(min, max) {
      return Math.random() * (max - min) + min;
    }
    var date1 = date1 || '01-01-1970'
    var date2 = date2 || new Date().toLocaleDateString()
    date1 = new Date(date1).getTime()
    date2 = new Date(date2).getTime()
    if( date1>date2){
        return new Date(randomValueBetween(date2,date1))   
    } else{
        return new Date(randomValueBetween(date1, date2)) 

    }
}



//9



let result = []
for (let i = 0; i < dtoIn.count; i++) {
    //9.1

    let person = {
      gender: gender[getRandom(1, gender.length) - 1],
      workload: workload[getRandom(1, workload.length) - 1],
      birthdate: randomDate(minDatum, maxDatum)
    }
    
  
  if (person.gender === "male"){

    person["name"] = nameM[getRandom(1, nameM.length) - 1]
    person["surname"] = surnameM[getRandom(1, surnameM.length) - 1]
    if (!person['surname'])
      console.log(person)
    
    
  }
  else {
    
    person["name"] = nameF[getRandom(1, nameF.length) - 1]
    person["surname"] = surnameF[getRandom(1, surnameF.length) - 1]
    
  }

    if (!person.surname) {
      console.log(person)
    }
  result.push(person)
  
}
return result







}
//@@viewOff:main

