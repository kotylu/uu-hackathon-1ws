const getBornDate = (person) => (new Date(person.birthdate));


const calculateAge = (person) => {
  const currDate = new Date();
  let age = currDate.getFullYear() - getBornDate(person).getFullYear();
  if ((getBornDate(person).getMonth() < currDate.getMonth())
    || (getBornDate(person).getMonth() === currDate.getMonth() && getBornDate(person).getDate() < currDate.getDate())) {
    age--;
  }
  return age;
};

const getMedian = (list, value) => {
  if (list.length % 2 === 0) {
    let sum = 0;
    const mids = list.splice(list.length/2, 2).map(item => {sum += item[value]})
    return sum /2;
  }
  else {
    return list[Math.ceil(list.length/2)][value]
  }
}

const getMedian = (list) => {
  if (list.length % 2 === 0) {
    let sum = 0;
    const mids = list.splice(list.length/2, 2).map(item => {sum += item})
    return sum /2;
  }
  else {
    return list[Math.ceil(list.length/2)]
  }
}

const getAvg = (list) => {
  const sum =0;
  list.map(item => {
    sum += item
  })
  return sum / list.length;
}

  //1
const result = {
  total,
  avergeAge,
  medianAge,
  minAge,
  maxAge,
  medianWorkload,
  womanAvergeWorkload 

}

const womenCount = 0;
  dtoIn.forEach(item => {
    const age = calculateAge(item.birthdate);
    if (!minAge || result["minAge"] > age) {
      result["minAge"] = age;
    }
    if (!maxAge || result["maxAge"] < age) {
      result["maxAge"] = age;
    }
    result["total"] += 1;
    result[`avergeAge`].push(age)
    if (item.gender ==== "female") {
      womenCount++;
      if (!result["womanAvergeWorkload"]) {
        result["womanAvergeWorkload"] = 0;
      }
      result["womanAvergeWorkload"] += item.workload;
    }

  });



  result = {
    ...result,
    womanAvergeWorkload: result["womanAvergeWorkload"] / womenCount,
    medianAge: getMedian(result["avergeAge"]),
    avergeAge: getAvg(result["avergeAge"]),
    medianWorkload: getMedian(dtoIn, "workload")

  }

  return result;

