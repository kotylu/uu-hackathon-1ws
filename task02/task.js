//@@viewOff:let
//@@viewOff:let
let getBornDate = (person) => (new Date(person.birthdate));

let calculateAge = (person) => {
  let currDate = new Date();
  let age = currDate.getFullYear() - getBornDate(person).getFullYear();
  if ((getBornDate(person).getMonth() < currDate.getMonth())
    || (getBornDate(person).getMonth() === currDate.getMonth() && getBornDate(person).getDate() < currDate.getDate())) {
    age--;
  }
  return age;
};

let getMedianObj = (list, value) => {
  return getMedian(list, (item => item[value]), (item => item[value]));
}

let getMedianList = (list) => {
  return getMedian(list, (item => item), (item => item));
}

let getMedian = (list, sumFunc, single) => {
  if (list.length === 2) {
    let sum = 0;
    list.splice(list.length/2-1, 2).map(item => {sum += sumFunc(item)})
    return sum /2;

  }
  else if (list.length % 2 === 0) {
    let sum = 0;
    let mids = list.splice(list.length/2, 2).map(item => {sum += sumFunc(item)})
    return sum /2;
  }
  else {
    return single(list[Math.ceil(list.length/2)])
  }
}
//@@viewOn:helpers
//@@viewOff:helpers

//@@viewOn:main
/**
 * @param {object} dtoIn input data
 * @return {object} output data
**/
function main(dtoIn=[]) {

  //1
let result = {
  total: 0,
  avergeAge: [],
  medianAge: undefined,
  minAge: undefined,
  maxAge: undefined,
  medianWorkload: undefined,
  womanAvergeWorkload: undefined 

};
const avgComputer = {
  ageSum: 0,
  ageCnt: 0,
  womanLoadSum: 0,
  womanLoadCnt: 0
};

// #TODO sort and set result in one loop
// then min and max are just first and last obj
dtoIn.forEach(item => {
  let age = calculateAge(item);
  avgComputer.ageSum += age;
  avgComputer.ageCnt++;
  if (!result["minAge"] || result["minAge"] > age) {
    result["minAge"] = age;
  }
  if (!result["maxAge"] || result["maxAge"] < age) {
    result["maxAge"] = age;
  }
  result["total"] += 1;
  if (item.gender === "female") {
    avgComputer.womanLoadSum += item.workload;
    avgComputer.womanLoadCnt++;
  }

});



  result = {
    ...result,
    avergeAge: avgComputer.ageSum / avgComputer.ageCnt,
    womanAvergeWorkload: avgComputer.womanLoadSum / avgComputer.womanLoadCnt,
    //medianAge: getMedianList(),
    //medianWorkload: getMedianObj(dtoIn, "workload")

  }

  return result;
}
//@@viewOff:main