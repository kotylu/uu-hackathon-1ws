//@@viewOff:const
//@@viewOff:const

//@@viewOn:helpers
//@@viewOff:helpers

const bornThisMonth = (person) => (getBornDate(person).getMonth() === new Date().getMonth());

const getBornDate = (person) => (new Date(person.birthdate));

const calculateAge = (person) => {
  const currDate = new Date();
  let age = currDate.getFullYear() - getBornDate(person).getFullYear();
  //if ((getBornDate(person).getMonth() < currDate.getMonth())
    //|| (getBornDate(person).getMonth() === currDate.getMonth() && getBornDate(person).getDate() < currDate.getDate())) {
    //age--;
  //}
  return age;
};

//@@viewOn:main
/**
 * @param {object} dtoIn input data
 * @return {object} output data
 * kteří zaměstnanci mají narozeniny v měsíci, ve kterém je program spušten.
 * {
    gender: "male",
    birthdate: "2000-08-07T00:00:00.000Z",
    name: "Jan",
    surname: "Novák"
  },
**/
function main(dtoIn=[]) {
  const inputList = dtoIn;

  const result = [];
  inputList.map(person => {
    if (bornThisMonth(person)) {
      result.push({
        name: person.name,
        surname: person.surname,
        gender: person.gender,
        age: calculateAge(person)
      });
    }
  });

  return result;

}
//@@viewOff:main