//@@viewOff:const
//@@viewOff:const

const { mkdir } = require("fs");

//@@viewOn:helpers
//@@viewOff:helpers

const initResult = () => ({
  workload: {},
  age: {},
  maleLoad: {},
  femaleLoad: {}
});

const incrementItemCount = (name, group, object) => {
    if (!object[group][name.toLowerCase()]) {
      object[group][name.toLowerCase()] = 0;
    }
    object[group][name.toLowerCase()]++;
};

const isMale = (person) => (person.gender === "male");

const getBornDate = (person) => (new Date(person.birthdate));

const calculateAge = (person) => {
  const currDate = new Date();
  let age = currDate.getFullYear() - getBornDate(person).getFullYear();
  if ((getBornDate(person).getMonth() < currDate.getMonth())
    || (getBornDate(person).getMonth() === currDate.getMonth() && getBornDate(person).getDate() < currDate.getDate())) {
    age--;
  }
  return age;
};

const mergeMaleFemaleWorkloads = (result) => {
  const wkeys = []
  const female = Object.keys(result["femaleLoad"]).map(key => {
    if (!wkeys.includes(key))
      wkeys.push(key);
  });
  const male = Object.keys(result["maleLoad"]).map(key => {
    if (!wkeys.includes(key))
      wkeys.push(key);
  });



  return wkeys.map(key => ({
    label: key,
    valueMale: !result["maleLoad"][key] ? 0 : result["maleLoad"][key],
    valueFemale: !result["femaleLoad"][key] ? 0 : result["femaleLoad"][key]
  }));

}
//@@viewOn:main
/**
 * @param {object} dtoIn input data
 * @return {object} output data
 * 
 *  který pro vygenerovaný seznam zaměstnanců připraví data určená pro zobrazení v grafech:

 *  rozložení pracovních úvazků formou koláčového grafu
 *  četnost věku všech mužů formou sloupcového grafu
 *  četnost pracovních úvazků mužů a žen formou složeného sloupcového grafu
**/
function main(dtoIn=[]) {
  const inputList = dtoIn;

  const result = initResult();

  inputList.map(person => {
    incrementItemCount(new String(person.workload), "workload", result);
    incrementItemCount(new String(calculateAge(person)), "age", result);
    if (isMale(person)) {
      incrementItemCount(new String(person.workload), "maleLoad", result);
    }
    else {
      incrementItemCount(new String(person.workload), "femaleLoad", result);
    }
  });

  return {
    pieChart: Object.keys(result["workload"]).map(key => ({label: key, value: result["workload"][key]})),
    barChart: Object.keys(result["age"]).map(key => ({label: key, value: result["age"][key]})),
    stackedBarChart: mergeMaleFemaleWorkloads(result)
  };
}
//@@viewOff:main